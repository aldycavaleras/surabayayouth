<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * Class Upload_model
 *
 * Mengelola berbagai logika aplikasi dan database untuk modul upload file/gambar.
 * Saling melengkapi dengan media
 */
 
class Upload extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->uploadPath = "./spokenword/";
        $this->load->helper('directory');
    }
 
    /**
     * Upload single non zip file
     *
     * @param $fileName
     * @return array
     */
    function uploadFileSingle($fileName)
    {
 
        $config['upload_path']      = $this->uploadPath;
        $config['allowed_types']    = 'pdf|doc|docx|xls|xlsx|ppt|pptx|txt|jpg|jpeg|png|gif';
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('spokenword'))
        {
            $data = array(
                'code'  => 500,
                'data' => $this->upload->display_errors()
            );
        }
        else
        {
            $uploadData=$this->upload->data();
 
            $data = array(
                'code'  => 200,
                'data' => $uploadData
            );
        }
 
        return $data;
 
    }
 
}