<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Postmark
 * Untuk mengelola berbagai fungsi yang dibutuhkan untuk integrasi dengan Postmark.
 * Layanan pengiriman email transaksi seperti reset password dan welcome email.
 */

class Postmark {

    var $endpoint   = "https://api.postmarkapp.com/email";
    var $token      = "3774a734-56e0-43a8-84fa-bb70808bbd81";
    var $from       = "developer@surabayayouth.org";
    
    public function send($data)
    {

        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL             => $this->endpoint,
            CURLOPT_HTTPHEADER      => array(
                'Accept: application/json',
                'X-Postmark-Server-Token: '.$this->token,
                'Content-Type: application/json'
            ),
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_POST            => 1,
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPAUTH        => 1
        );

        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        return $result;

    }

    public function packData($to,$subject,$body){
        $data = array(
            'From'    => $this->from,
            'To'      => $to,
            'Subject' => $subject,
            'HtmlBody'=> $body
        );
        return json_encode($data);
    }

}