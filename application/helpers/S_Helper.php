<?php


function messageAlert($status,$label){

    $result="";
    if($status && $status=="ok"):
        $result.='<div class="alert alert-success alert-dismissable role="alert">';
        $result.='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        $result.='<strong>Thanks for submiting, we have send you an email.</strong> please check it!';
        $result.=' </div>';

   elseif($status && $status=="nok"):
        $result.=' <div class="alert alert-danger alert-dismissable role="alert">';
        $result.='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        $result.='<strong>Oops..</strong> Something wrong..';
        $result.=' </div>';

    elseif($status && $status=="welcome"):
        $result.=' <div class="alert alert-dark alert-dismissable">';
        $result.='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        $result.='<strong>Welcome guest!</strong> Please fill the form!';
        $result.=' </div>';

    elseif($status && $status=="fine"):
        $result.=' <div class="alert alert-dark alert-dismissable">';
        $result.='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        $result.='<strong>Success</strong>';
        $result.=' </div>';
   endif;
   return $result;

}

/**
 * Debugging
 *
 * @param $data
 * @param bool $exit
 */
function debug( $data,$exit=true,$format="array" ) {

    echo "<pre>";
    if($format=="json"){
        echo json_encode($data);
    }else{
        var_dump($data);
    }

    echo"</pre>";
    if($exit)
    {
        exit;
    }

}
function checklogin(){
	$ci =& get_instance();
	if ( ($ci->session->userdata('admin') ==='login')){
		redirect('/dashboard/dashboard');
	}
}
function checkAdmin($url = null,$redirect = true){
	$ci =& get_instance();
    if ( ($ci->session->userdata('admin') !=='login') && $redirect ){
        if ($url){
	        redirect($url);
        }else{
	        redirect('/dashboard');
        }
    }elseif($redirect){
	    redirect('/dashboard/dashboard');
    }
}

?>



