<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin", 'Madmin');
		$this->load->library('session');
	}

	public function index()
	{
		checklogin();
		$this->load->view('core/head');
		$this->load->view('auth/login');
		$this->load->view('core/foot');
	}

	public function doLogin()
	{
		$data  = $this->input->post();
		$user  = $data['username'];
		$pass  = $data['password'];
		$where = array(
			'username' => $user,
			'password' => $pass
			);
		$check = $this->Madmin->check($where);
		$q = $check->num_rows();
		if($q > 0){

		$data_session = array(
			'nama' => $user
			);
 
			$this->session->set_userdata('admin_session',$data_session);
			redirect('dashboard/dashboard');	
			
		}else{
			$this->load->view('core/head');
			$this->load->view('core/header');
			$this->load->view('auth/_login');
			$this->load->view('core/foot');
		}
	}

	public function dashboard()
	{
		checkAdmin(null,false);
		$data['participants'] = $this->Madmin->get();
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('admin/dashboard',$data);
		$this->load->view('core/foot');
	}

	public function edit($id)
	{
		if($id)
		{
			$participants    	  = $this->Madmin->getByID($id);
	        $data['last_id']      = $this->uri->segment(3);
	        $data['participants'] = $participants[0];
	       	$this->load->view('core/head');
			$this->load->view('core/header');
			$this->load->view('admin/edit',$data);
			$this->load->view('core/foot');
		} else {
			redirect('dashboard/dashboard?status=nok');
		}
	}

	public function editProcess()
	{
		$data = $this->input->post();
		$id   = $data['id'];
		if($data)
        {
            $q  = $this->Madmin->editProcess($id,$data);
            if($q){
                redirect('dashboard/dashboard?status=fine');
            }else{
                redirect('dashboard/dashboard?status=nok');
            }
        }
	}

	function deleteProcess($id)
    {
        if($id){
            $q = $this->Madmin->deleteProcess($id);
            if($q){
                redirect('dashboard/dashboard?status=fine');
            }else{
                redirect('dashboard/dashboard?status=nok');
            }
        }else{
            redirect('dashboard/dashboard');
        }
    }

    function doLogout(){
	    $this->session->sess_destroy();
	    redirect('dashboard');
    }

}