<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("crud", 'Mcrud');
	}


	public function index() 
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/index');
		$this->load->view('core/foot');
	}
	public function succeed() {
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/succeed');
		$this->load->view('core/foot');
	}

	public function people()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/people');
		$this->load->view('core/foot');
	}

	public function bas()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/bas');
		$this->load->view('core/foot');
	}

	public function processAdd() 
	{
		$this->load->library('postmark');
		$data    = $this->input->post();

		if($data){
			$activities = $data['activity'];
			unset($data['activity']);
			$q = $this->Mcrud->addProcess($data);
			$last_id = $this->db->insert_id();

			$dataNew=array();
			foreach ($activities as $row) {
				$dataNew[] = array(
				 'regis_code' => $last_id,
				 'activities' => $row,
				);
			}

			$codes = str_pad($last_id, 4, "0", STR_PAD_LEFT); 

			$this->db->insert_batch('seats', $dataNew);

			$name	 = $data['name'];
			$to   	 = $data['email'];
			$code    = $codes;
			$subject = 'Book a seat - SurabayaYouth';
			$body	 = 'Hi! '.$name.'!'. '<br><br>'.

						'Terima kasih untuk antusiasnya untuk menghadiri '.'<strong>Carnival 2018, Sabtu 24 Februari 2018.</strong>  '.'<br><br>'.
						'Kami tunggu kehadiranmu di <strong>Geldboom Food Plaza</strong> No. 89, Jl. Kalijudan, Jawa Timur 60114.  '.'<br><br>'.
						'Mohon untuk hadir tepat waktu, sesuai dengan topik talk show/workshop yang sudah kamu pilih sebelumnya. Oh iya, jangan lupa untuk tunjukkan e-mail ini ya di saat registrasi ulang tanggal 24 besok! '. '<br><br>'.
						
						'Kode registrasi : '.'<strong>'. $code. '</strong>'.'<br><br>'.

						'Sampai jumpa!';

			$email = $this->postmark->packData($to,$subject,$body);
			$send  = $this->postmark->send($email);

		}

		$config['upload_path']          = './spokenword/';
        $config['allowed_types']        = 'pdf|docx';

        $this->load->library('upload', $config);
		$uploader = $this->upload->do_upload('spokenword');
        if ( ! $uploader )
            {
               $error = array('error' => $this->upload->display_errors());
            }
        else
            {
            	$uploads = array('upload_data' => $this->upload->data());
            	$filename = $uploads['upload_data']['file_name'];
            	$newFileName = array('spokenword' => $filename); 
            	// var_dump($uploader);
            	// exit();
            	$this->db->where('id', $last_id);
            	$this->db->update('participants', array('spokenword' => $filename));
			}
		if($q){
			redirect('landing/succeed');
		} else {
			redirect('landing/bas?status=nok');
		}
		
	
	}


	
	public function sendMail($id)
	{


		if($send){
			redirect('landing/succeed');
		} else {
			redirect('landing/bas?status=nok');
		}

	}

	public function carnival()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/carnival');
		$this->load->view('core/foot');
	}

	public function program()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/program');
		$this->load->view('core/foot');
	}

	public function about()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/about');
		$this->load->view('core/foot');
	}

	public function contribute()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/contribute');
		$this->load->view('core/foot');
	}

	public function contact()
	{
		$this->load->view('core/head');
		$this->load->view('core/header');
		$this->load->view('landing/contact');
		$this->load->view('core/foot');
	}

}