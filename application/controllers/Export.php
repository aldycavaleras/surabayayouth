<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->load->library('session');
        // $this->thisSession = $this->sessionM->checkSession();

        //This page only for staff
        // if ($this->thisSession['admin_session'] != "SurabayaYouth") {
        //     redirect('/dashboard');
        // }

        $this->load->model('Admin', 'AdminM');
        $this->load->model('Crud', 'crudM');

        $participants = $this->tblPar = "`participants`";
        $seats        = $this->tblSeats = "`seats`";

    }

    function exportSql(){

        // $boothPending = $this->boothData($verification);
         $participants = $this->db->get($this->tblPar);
         $seats = $this->db->get($this->tblSeats);

        /** PHPExcel */
        include './application/vendor/phpoffice/phpexcel/Classes/PHPExcel.php';

        /** PHPExcel_Writer_Excel2007 */
        include './application/vendor/phpoffice/phpexcel/Classes/PHPExcel/Writer/Excel2007.php';

        $filename = "syouth-registant-data-".date("Ymdhis").".xlsx";

        $objectPHPExcel = new PHPExcel();
        $objectPHPExcel->getProperties()->setCreator("Surabaya Youth");
        $objectPHPExcel->getProperties()->setTitle("Surabaya Youth: Registered User");

        $objectPHPExcel->setActiveSheetIndex(0);

        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, "Regis code");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 1, "Name");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 1, "Jenis Kelamin");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 1, "Umur");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 1, "Jenjang");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 1, "Instagram");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 1, "Email");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, 1, "Phone");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, 1, "Activities");
        $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, 1, "Spoken Word");

        $this->db->select('*');
        $this->db->from('seats');
        $this->db->join('participants', 'seats.regis_code = participants.id');
        $query = $this->db->get();
        $result = $query->result_array();

        $i=1;
        $no=0;
        foreach($result as $registants){
            $i++;
            $no++;

            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $i, $registants['regis_code']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $i, $registants['name']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $i, $registants['jk']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $i, $registants['umur']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $i, $registants['jenjang']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $i, $registants['instagram']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $i, $registants['email']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(7, $i, $registants['phone']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(8, $i, $registants['activities']);
            $objectPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(9, $i, $registants['spokenword']);

        }

        $objectWritter = new PHPExcel_Writer_Excel2007($objectPHPExcel);
        $objectWritter->save("./export/".$filename);
        redirect("./export/".$filename);

    }

    /**
     * Booth pending data
     *
     * @return mixed
     */
    // function registant_data(){

    //     // ambil data
    //     $this->crudM->mydb->params = array(
    //         'participants' => $this->tblPar,
    //         'seats' => $this->tblSeats 
    //     );

    //     $participants = $this->crudM->mydb->get();

    //     $this->db->select('*');
    //     $this->db->from('seats');
    //     $this->db->join('participants', 'participants.id = seats.regis_code');

    //     $query = $this->db->get();
    //     $data = $query->result_array();
    //     $data['list'] = $this->CrudM->mydb->get();

    //     return $data;

    // }

}