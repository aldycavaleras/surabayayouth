
<section class="people">
  <h1 class="text-center mt">Our Team.</h1>
  <div class="people-container">
  <div class="row-wrapper">
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay black"></div>
        
          <img src="http://joyfuljourneyhotsprings.com/wp-content/uploads/2013/08/photodune-4276142-smiling-portraits-xl_031.jpg">

        <div class="photo-overlay-title">
          <div class="photo-overlay-text-title">
            <h2>Roger</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>Founder</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay black"></div>
          
          <img src="http://sitesao.com/phoenix/creative/wp-content/uploads/2014/09/member-1-600x600.png">
        
        <div class="photo-overlay-title">
        <div class="photo-overlay-text-title">
            <h2>Liam</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>CEO</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay black"></div>
        
    <!--  <img src="https://lh6.ggpht.com/CchgYI97bE4Pxpb7vhpcxKFe-edKrsl5Xxlb8HwY5G9PqcVw5_JnuuRIgZDaSIIbsMM=h900" class="fade-reveal"> -->
          <img src="http://sitesao.com/phoenix/default/wp-content/uploads/2014/09/member-3-600x600.png">

        <div class="photo-overlay-title">
        <div class="photo-overlay-text-title">
            <h2>Meep</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>Director</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>