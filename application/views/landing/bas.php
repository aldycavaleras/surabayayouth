
  <div class="container">
     
    <div class="row mt fw justify-content-md-center">

        <div class="col-md-12">
          <form action="<?=base_url('landing/processAdd')?>" method="post" enctype="multipart/form-data"> <!-- Untuk message alert-->

          <label for="name">* Nama. </label> <br>
          <input type="text" name="name" class="contact-form" placeholder="Nama">
        </div>
        <div class="col-md-6">
           <label for="jk">Jenis kelamin.</label> <br>
          <select name="jk" class="activity" style="margin: 20px 0">
            <option value="">Select</option>
            <option value="Lelaki">Lelaki</option>
            <option value="perempuan">Perempuan</option>
          </select>
        </div>
        <div class="col-md-6">
           <label for="umur">Umur.</label> <br>
          <input type="number" name="umur" class="contact-form" placeholder="Umur">
        </div>
        <div class="col-md-12">
           <label for="jenjang">* Sekolah/Instansi.</label> <br>
          <input type="text" name="jenjang" class="contact-form" placeholder="Sekolah/Instansi" required>
        </div>
        <div class="col-md-12">
           <label for="phone">* No. Hp.</label> <br>
         <input type="text" name="phone" class="contact-form" placeholder="Phone Number" value="+62" required>
        </div>
        <div class="col-md-12">
           <label for="instagram">Instagram.</label> <br>
          <input type="text" name="instagram" class="contact-form" placeholder="Instagram" value="@">
        </div>
        <div class="col-md-12">
           <label for="mail">* Email.</label> <br>
          <input type="email" name="email" class="contact-form" placeholder="Email" required>
        </div>
        

        <div class="col-md-12">
          <div class="mo">
          <strong for="activity">* Aktivitas.</strong> <br>
          <strong>talkshow.</strong><br>
          <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Sustainable Cities and Communities"> <span>Sustainable Cities and Communities (11.30)</span></label> <br>
           <span class="mentor">
            <ol>
             <li>Raharto Teno (Wakil Walikota Pasuruan)</li>
             <li>Ayos Purwoaji (Penulis dan Kurator Independen)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Decent Work and Economic Growth"> <span>Decent Work and Economic Growth (13.00)</span></label>
           <br>
           <span class="mentor">
            <ol>
             <li>Ricky Pesik (Wakil Kepala BEKRAF RI)</li>
             <li>Serikat Sindikasi (Serikat Pekerja Media dan Industri Kreatif)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Good Health and Well Being"> <span>Good Health and Well Being (14.30)</span></label> <br>
           <span class="mentor">
            <ol>
             <li>Ratu (Surabaya Sehat)</li>
             <li>Eva (Garda Pangan)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Gender Equality"> <span>Gender Equality (16.30)</span></label> <br>
           <span class="mentor"> 
             <ol>
              <li>Poedjiati Tan  (Aktivis Gender Equality)</li>
             </ol>
           </span>
           <br>
           <strong>workshop.</strong><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Photographic Film Processing with Analog Soerabaja"> <span>Photographic Film Processing with Analog Soerabaja (10.00 WIB-selesai)</span></label><br><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Ultimate Guide to be Content Creator with Naufal Zuhdi"> <span>Ultimate Guide to be Content Creator with Naufal Zuhdi (12.00 - selesai)</span></label><br><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="3D Animation Still Life with Yuwanda Pratama"> <span>3D Animation Still Life with Yuwanda Pratama (18.00 - selesai)</span></label>
           <br><br>

           <label for="lsw">
            <input type="checkbox" name ="activity[]" class="checkbox" value="Lit Spoken Word" id="lsw" onclick="ShowHideDiv(this)" />
                Lit Spoken Word
            </label>
            <div id="spokenwords" style="display: none">
               Upload karyamu: <br>
               <?php echo form_open_multipart('upload/do_upload');?>
              <input type="file" accept="application/pdf" name="spokenword">
            </div>

        </div></div>

        <span style="margin: 10px 10px; color: rgba(0,0,0,.5);">* Required.</span>
          <input type="submit" class="contact-form-submit" value="Process">
        </form>
   
  </div> 
</div>
</div>