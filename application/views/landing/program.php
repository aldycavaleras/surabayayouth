

<section id="program" class="program">
    <center> <img src="<?=base_url()?>assets/images/logo.png" alt="logo"></center>

    <div class="container-fluid">
      <h1 class="program-heading">Program</h1>
    </div>

    <div class="left-program">
      <div class="container left-text">
        <h1>Content</h1>
        <span>Workshop by</span> <strong>Hand Meet Hand</strong><br><br>
        <p>Bagi anak muda Surabaya yang memiliki minat dalam bidang industri kreatif, Hand Meet Hand
            menyediakan platform di mana mereka dapat belajar dengan mengikuti workshop, langsung
            dengan mentor-mentor yang ahli di bidangnya masing-masing seperti musik, kerajinan, film, dll.</p>
      </div>
    </div>
    <div class="right-program">
      <div class="container right-text">
        <h1>Literaturia</h1>
        <p>Dengan Mengusung konsep lain dibandingkan dengan talkshow pada umumnya. Chatterbus,
            sesuai namanya, dilakukan di dalam bis yang sedang berjalan mengelilingi spot-spot bersejarah
            di Surabaya. Selain mendapatkan ilmu, peserta juga mendapatkan pengetahuan-pengetahuan
            baru soal kota yang selama ini mereka tinggali; yaitu Surabaya.</p>
      </div>
    </div>
    <div class="left-program">
      <div class="container left-text">
        <h1>Talks</h1>
        <p>Dengan SGD (Sustainable Goals Development) sebagai tema besar Surabaya Youth Carnival
            tahun ini, kami mengundang beberapa tokoh tanah air untuk berbicara tentang isu-isu yang
            kami angkat tahun ini. Harapannya, semakin banyak orang yang sadar tentang betapa
            pentingnya isu SGD dan apa yang anak muda Surabaya bisa lakukan untuk mewujudkannya.</p>
      </div>
    </div>
    <div class="right-program">
      <div class="container right-text">
        <h1>Carnival</h1>
        <p>Surabaya Youth Carnival merupakan sebuah program tahunan yang diadakan oleh Surabaya
            Youth untuk mengubah kondisi komunitas-komunitas di Surabaya. Pada saat itu,
            komunitas-komunitas di Surabaya cenderung berjalan sendiri-sendiri. Kondisi itu menyebabkan
            jarangnya kolaborasi yang terjadi di antara mereka. Untuk mengubah semua itu, Surabaya
            Youth Carnival hadir dengan misi mengajak komunitas-komunitas tersebut untuk berkolaborasi
            dengan satu sama lain. Kurangnya kegiatan positif di kalangan anak muda Surabaya juga
            menjadi faktor lain diadakannya Surabaya Youth Carnival.</p>
      </div>
    </div>


  </section>