

  <section id="about" class="about">
    <div class="about-bg">
    <center> <img src="<?=base_url()?>assets/images/logo.png" alt="logo"></center>
      <span>About</span>
    </div>
    <div class="about-text">
      <div class="divider"></div>
        <h1>What we dream</h1>
        <p>Surabaya Youth adalah sebuah gerakan independen yang fokus dalam memberdayakan
            pemuda, sekaligus meningkatkan partisipasi mereka dalam membantu pembangunan
            berkelanjutan di Indonesia. Didirikan pada tahun 2013, Surabaya Youth bergerak dengan dasar
            pemikiran kreatif dari seluruh anggotanya. Surabaya Youth memiliki satu tujuan utama, yaitu
            mengajak sebanyak mungkin anak muda untuk menjadi pemuda yang lebih baik. Dalam
            menjalankan pergerakan kami, ada empat misi penting yang harus selalu kami sebarluaskan,
            yaitu menghubungkan, berjejaring, berkolaborasi dan mewakili satu sama lain. Salah satu dari
            sekian banyak gagasan Surabaya Youth adalah menjadi tempat bagi pemuda untuk berkumpul
            dan memperluas koneksi mereka dengan berdiskusi soal isu-isu yang harus diperbaiki di
            Surabaya. Kami percaya bahwa, bersama, Surabaya Youth dapat mencapai mimpi-mimpi
            tersebut sehingga kami dapat membuat Surabaya menjadi kota yang lebih nyaman untuk
            ditinggali di kemudian hari.
        </p>
    </div>
  </section>

  <section id="contact" class="contact">
    <div class="contact-container">
      <div class="row">
        <div class="col-md-2" style="padding-top: 50px;">
          <div class="divider2"></div>
          <span class="ct">
            Contact. <br>
          <a href="#"><i class="fa fa-comments-o"></i></a>
          </span>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-8 mt">

          <h1>We're here for you</h1>
          <div class="row">
            <div class="col-md-6 mt">
              Phone: <a href="tel:+6281297649535">+6281297649535</a> (Merry) <br>
              <div class="mt">Line: @jxi3907x</div>
            </div>
            <div class="col-md-6 mt">
              Email: <a href="mailto:mail@surabayayouth.org">surabayayouth.org</a> 
            </div>
          </div>

        </div>

      </div>
    </div>
  </section>