<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Surabaya Youth</title>

    <!-- stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/profile.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bas.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap-select.min.css">

    <!-- Datatable Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.bootstrap4.min.css">

  </head>