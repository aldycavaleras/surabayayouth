<body>

  <!-- NAVBAR -->
  <nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container-fluid">

      <a class="navbar-brand" href="<?=base_url()?>landing/index">
        <img src="<?=base_url()?>assets/images/logo.png" width="100" alt="">
      </a>
      <p>Be a Better Youth</p>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse main-navbar" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>landing/about">About</a>
          </li>
          <li class="nav-item">
            <!-- sementara -->
            <a class="nav-link" href="<?=base_url()?>landing/about#contact">People</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>landing/program">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>landing/contribute">Contribute</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>landing/carnival">Carnival</a>
          </li>
          <?php if ( $_SESSION['admin_session'] ) : ?>
            <li class="nav-item">
              <a class="nav-link" href="<?=base_url()?>dashboard/dashboard">Dashboard</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?=base_url()?>dashboard/doLogout">Logout</a>
            </li>
          <?php endif; ?>

        </ul>
      </div>

    </div>
  </nav>