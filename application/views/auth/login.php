<section class="login">  

  <div class="row">
    <div class="container">
      <div class="col-sm-6 mx-auto">
        <div class="card">
          <div class="card-header text-center black-header">
            Login
          </div>
          <div class="card-body">
            <h5 class="card-title text-center">Login To Continue</h5>
            <?php echo form_open('dashboard/doLogin'); ?>
            <form action="<?=base_url();?>dashboard/doLogin" method="post">
              <div class="form-group row mx-auto">
                <label for="inputUsername3" class="col-sm-3 col-form-label">Username</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputEmail3" placeholder="username" name="username">
                </div>
              </div>
              <div class="form-group row mx-auto">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
                </div>
              </div>
              <div class="form-group row mx-auto">
                <div class="col-sm-12">
                  <button type="submit" class="btn btn-dark btn-lg btn-block">Sign in</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>



    </div>
  </div>
</section>