<?php 
	$this->load->library('session');
	
	$this->session->user_data;
	if ( !isset( $_SESSION['admin_session'] ) ) : ?>
		
		<center style="margin: 100px auto;">
			<h2>Access Required</h2>
			<p>Maaf untuk mengakses halaman ini anda perlu memiliki akses khusus terlebih dahulu.</p>
		</center>
		
 	<?php else : ?>

<section class="admin" style="margin: 0 50px;">
	<div class="row">
		<div class="container-fluid">
			<div class="col-sm-12 rada-handap">
				<h1>Dashboard</h1>
				<div class="card-deck">
					<div class="card text-white bg-info"">
					  <div class="card-body">
					    <h5 class="card-title">Jumlah Peserta</h5>
					    <p class="card-text">
					    	<?php $query = $this->db->query('SELECT id FROM participants');
								echo $query->num_rows(); ?>
					    </p>
					    <button type="button" class="export" onclick="window.location='<?= site_url("export/exportSql");?>'">Export Data</button>
					  </div>
					</div>
					<!-- <div class="card text-white bg-dark"">
					  <div class="card-body">
					    <h5 class="card-title">Jumlah Peserta Workshop</h5>
					    <p class="card-text"></p>
					  </div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container-fluid">
			<div class="col-sm-12 rada-handap">
				<table id="example" class="table table-bordered table-stripped" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>Name</th>
			                <th>Phone</th>
			                <th>Instagram</th>
			                <th>Email</th>
			                <th>Activity</th>
			                <th>Registration Code</th>
			                <th>Action</th>
			            </tr>
			        </thead>	
			        <tbody>	        
			            <?php foreach ($participants as $row){
			            	?>
                        <tr>
                            <td><?=$row['name']?></td>
                            <td><?=$row['phone']?></td>
                            <td><?=$row['instagram']?></td>
                            <td><?=$row['email']?></td>
                            <td><?=$row['activities']?></td>
                            <td><?=$row['regis_code']?></td>
                            <td>
                              <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
							    <div class="dropdown-menu">
							      <a class="dropdown-item" href="#" data-toggle="modal" data-target=".bd-example-modal-sm">Delete</a>
							    </div>
                            </td>
			            </tr>
			            <?php } ?>
			        </tbody>
			    </table>
			</div>
		</div>
	</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-sm " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         Yakin ingin menghapus data dengan ID <?=$participants[0]['id'];?> ?
      </div>
      <div class="modal-footer">
      	<a class="btn btn-danger" href="<?=base_url('dashboard/deleteProcess/'.$participants[0]['id'])?>">Hapus</a>
        <a class="btn btn-dark text-white" data-dismiss="modal">Batalkan</a>
        
      </div>
    </div>
  </div>
</div>

</section>
<?php endif; ?>