

<?php //$jk = $participants['jenis_kelamin']; ?>
  <div class="container">
     
    <div class="row mt fw justify-content-md-center">

        <div class="col-md-12">
          <form action="<?=base_url('dashboard/editProcess')?>" method="post"> <!-- Untuk message alert-->

          <label for="name">* Nama. </label> <br>
          <input type="text" name="name" class="contact-form" placeholder="Nama" value="<?=$participants['name']?>">
          <input type="text" name="id" class="contact-form" value="<?=$last_id?>">
        </div>
        <div class="col-md-6">
           <label for="jk">Jenis kelamin.</label> <br>
          <select name="jk" class="activity" style="margin: 20px 0">
            <option value="">Select</option>
            <option value="Lelaki">Lelaki</option>
            <option value="perempuan">Perempuan</option>
          </select>
        </div>
        <div class="col-md-6">
           <label for="umur">Umur.</label> <br>
          <input type="number" name="umur" class="contact-form" placeholder="Umur" value="">
        </div>
        <div class="col-md-12">
           <label for="jenjang">* Sekolah/Instansi.</label> <br>
          <input type="text" name="jenjang" class="contact-form" placeholder="Sekolah/Instansi" value="">
        </div>
        <div class="col-md-12">
           <label for="phone">* No. Hp.</label> <br>
         <input type="text" name="phone" class="contact-form" placeholder="Phone Number" value="<?=$participants['phone']?>">
        </div>
        <div class="col-md-12">
           <label for="instagram">Instagram.</label> <br>
          <input type="text" name="instagram" class="contact-form" placeholder="Instagram" value="<?=$participants['instagram']?>">
        </div>
        <div class="col-md-12">
           <label for="mail">* Email.</label> <br>
          <input type="email" name="email" class="contact-form" placeholder="Email" value="<?=$participants['email']?>">
        </div>
        

        <div class="col-md-12">
          <div class="mo">
          <strong for="activity">* Aktivitas.</strong> <br>
          <strong>talkshow.</strong><br>
          <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Sustainable Cities and Communities" disabled> <span>Sustainable Cities and Communities (11.30)</span></label> <br>
           <span class="mentor">
            <ol>
             <li>Raharto Teno (Wakil Walikota Pasuruan)</li>
             <li>Ayos Purwoaji (Penulis dan Kurator Independen)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Decent Work and Economic Growth" disabled> <span>Decent Work and Economic Growth</span></label>
           <br>
           <span class="mentor">
            <ol>
             <li>Ricky Pesik (Wakil Kepala BEKRAF RI)</li>
             <li>Serikat Sindikasi (Serikat Pekerja Media dan Industri Kreatif)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Good Health and Well Being" disabled> <span>Good Health and Well Being</span></label> <br>
           <span class="mentor">
            <ol>
             <li>Ratu (Surabaya Sehat)</li>
             <li>Eva (Garda Pangan)</li>
            </ol>
           </span>
           <br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Gender Equality" disabled> <span>Gender Equality</span></label> <br>
           <span class="mentor"> 
             <ol>
              <li>Poedjiati Tan  (Aktivis Gender Equality)</li>
             </ol>
           </span>
           <br>
           <strong>workshop.</strong><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Photographic Film Processing with Analog Soerabaja" disabled> <span>Photographic Film Processing with Analog Soerabaja</span></label><br><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="Ultimate Guide to be Content Creator with Naufal Zuhdi" disabled> <span>Ultimate Guide to be Content Creator with Naufal Zuhdi</span></label><br><br>
           <label>
           <input type='checkbox' name='activity[]' class="checkbox" id="ka" value="3D Animation Still Life with Yuwanda Pratama"> <span>3D Animation Still Life with Yuwanda Pratama</span></label>

        </div></div>
          <input type="submit" class="contact-form-submit" value="Process">
        </form>
   
  </div> 
</div>
</div>