<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->tblPar = "`participants`";
		$this->tblSeats = "`seats`";
	}

	// function regis_code()
	// {
	// 	$this->db->select('RIGHT(participants.regis_code,2) as code', FALSE);
	//     $this->db->order_by('regis_code','DESC');
	//     $this->db->limit(1);
	//     $query = $this->db->get($this->tblPar);

	//     if($query->num_rows() <> 0){
	//       $data = $query->row();
	//       $code = intval($data->code) + 1;
	//     }else{
	//       $code = 1;
	//     }
	//     $kodemax = str_pad($code, 2, "0", STR_PAD_LEFT); 
	//     $regis_code = $kodemax;

	// }

	function get()
	{
		$q = $this->db->get($this->tblPar);
		$resultDb = $q->result_array();
		return $resultDb;
	}

	function getByID($id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get($this->tblPar);
		$result = $this->result_array($q);
		return $result;
	}

	function addProcess($data)
	{
		 $q = $this->db->insert($this->tblPar, $data);
		 $last_id = $this->db->insert_id();
		 return $last_id;
	}

}