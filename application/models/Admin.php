<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->tblLog = "`login`";
		$this->tblPar = "`participants`";
		$this->allTbl = "`participants, seats`";		
		$this->IdPar  = "`id`";
		$this->tblSeats = "`seats`";
		// $this->regis_code = "`regis_code`";

	}


	public function check($where)
	{
//		var_dump($where);
		$q = $this->db->get_where($this->tblLog,$where);
//		echo $this->db->last_query();
		return $q;
	}

	public function get()
	{
		$this->db->select('*');
		$this->db->from('seats');
		$this->db->join('participants', 'seats.regis_code = participants.id');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;

		// $q = $this->db->get($this->tblPar);
		
		// $resultDb = $q->result_array();
		// return $resultDb;
	}

	public function getByID($id)
	{
		$q = $this->db->get_where($this->tblPar, array('id' => $id) );
		$resultDb = $q->result_array();
		return $resultDb;
	}

	public function editProcess($id,$data)
	{
		$this->db->where($this->IdPar, $id);
        $q = $this->db->update($this->tblPar, $data);
        return $q;
	}

	public function deleteProcess($id)
	{
        $this->db->where('id', $id);
        $q = $this->db->delete($this->tblPar);
        if ($q) {
        	$this->db->where('regis_code', $id);
        	$this->db->delete($this->tblSeats);
        }
        return $q;
	}


}