-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 19, 2018 at 01:51 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surd6749_surabayayouth`
--

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `umur` int(2) NOT NULL,
  `jenjang` varchar(100) NOT NULL,
  `instagram` varchar(35) NOT NULL,
  `email` varchar(35) NOT NULL,
  `phone` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `name`, `jk`, `umur`, `jenjang`, `instagram`, `email`, `phone`) VALUES
(1, 'Sevira Marsanti Utari', 'perempuan', 21, 'Universitas Airlangga', '@sasainsane', 'seviramarsantiutari@gmail.com', '085695881638'),
(2, 'Rosiana', 'perempuan', 22, 'STID AL HADID', '@imociro', 'iamrosiana@gmail.com', '+62852228442'),
(3, 'Farah Afifah Effendi', 'perempuan', 20, 'Politeknik Perkapalan Negeri Surabaya', '@farafifah', 'hiimfarafifah@gmail.com', '+62822131889'),
(4, 'Sholichatur rizkiyah', 'perempuan', 19, 'Politeknik Elektronika negeri surabaya', '@sholichatur_rizki', 'sholichatur.rizkiyah@gmail.com', '+62813670534'),
(5, 'sholichatur rizkiyaj', 'perempuan', 19, 'Politeknik elektronika negeri surabaya', '@sholichatur_rizki', 'sholichatur.rizkiyah@gmail.com', '+62813670534'),
(6, 'Dinda Ajeng W', 'perempuan', 20, 'Institut Teknologi Sepuluh Nopember Surabaya', '@dindaajw', 'dajengwindiana@gmail.com', '+62822187732'),
(7, 'dinda ajeng w', 'perempuan', 20, 'ITS', '@dindaajw', 'dajengwindiana@gmail.com', '+62822187732'),
(8, 'Rizky fitria muliawati', 'perempuan', 20, 'Politeknik perkapalan negeri surabaya', '@rizkyfitriamuliawati', 'rizkyfitriamuliawati@gmail.com', '+62858564881'),
(9, 'FACHRIZA PUTRA PRATAMA', 'Lelaki', 18, 'Universitas Jember', '@fachriza_p', 'fachrizaputra05@gmail.com', '+62857060245'),
(10, 'Radja ahmad ertansyah gani', 'Lelaki', 20, 'Universitas negeri surabaya', '@Radjagani', 'radja.zr@gmail.com', '+62812354616'),
(11, 'Aldy Cavalera', 'Lelaki', 17, 'SMKN Situraja', '@aldycavalera', 'personal.cavalera@gmail.com', '+62852175689');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
