<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Surabaya Youth</title>

    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/profile.css">
    <link rel="stylesheet" href="assets/css/bas.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  </head>
  <body style="background: black">

  <!-- NAVBAR -->
  <nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container-fluid">

      <a class="navbar-brand" href="#">
        <img src="assets/images/logo.png" width="100" alt="">
      </a>
      <p>Be a Better Youth</p>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse main-navbar" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="about.php">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="people.php">People</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="program.php">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contribute.php">Contribute</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="carnival.php">Carnival</a>
          </li>
        </ul>
      </div>

    </div>
  </nav>


<section class="people">
  <h1>Our Team.</h1>
  <div class="people-container">
  <div class="row-wrapper">
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay iron"></div>
        
          <img src="http://joyfuljourneyhotsprings.com/wp-content/uploads/2013/08/photodune-4276142-smiling-portraits-xl_031.jpg">

        <div class="photo-overlay-title">
          <div class="photo-overlay-text-title">
            <h2>Roger</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>Founder</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay cap"></div>
          
          <img src="http://sitesao.com/phoenix/creative/wp-content/uploads/2014/09/member-1-600x600.png">
        
        <div class="photo-overlay-title">
        <div class="photo-overlay-text-title">
            <h2>Liam</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>CEO</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
    <div class="photo-box-column">
      <div class="photo-box-reveal">
        <div class="color-overlay hulk"></div>
        
    <!--  <img src="https://lh6.ggpht.com/CchgYI97bE4Pxpb7vhpcxKFe-edKrsl5Xxlb8HwY5G9PqcVw5_JnuuRIgZDaSIIbsMM=h900" class="fade-reveal"> -->
          <img src="http://sitesao.com/phoenix/default/wp-content/uploads/2014/09/member-3-600x600.png">

        <div class="photo-overlay-title">
        <div class="photo-overlay-text-title">
            <h2>Meep</h2>
          </div>
        </div>
        <div class="photo-overlay">
          <div class="photo-overlay-text">
          <h3>Director</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</p></div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>



    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </body>
</html>
