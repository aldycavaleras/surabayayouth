<?php require_once 'header.php'; ?>


  <div class="container">
     
    <div class="row mt">
     

        <div class="col-md-12">
          <form action="" method="post">
          <label for="name">Nama. </label> <br>
          <input type="text" name="name" class="contact-form" placeholder="Name">
        </div>
        <div class="col-md-12">
           <label for="num">* No. Hp.</label> <br>
         <input type="text" name="num" class="contact-form" placeholder="Phone Number" value="+62" required="">
        </div>
        <div class="col-md-12">
           <label for="instagram">Instagram.</label> <br>
          <input type="text" name="instagram" class="contact-form" placeholder="Instagram" value="@">
        </div>
        <div class="col-md-12">
           <label for="email">* Email.</label> <br>
          <input type="email" name="email" class="contact-form" placeholder="Email" required="">
        </div>
        <div class="col-md-12">
          <label for="email">* Aktivitas.</label> <br>
          <select name="activity" class="activity" required="">
            <option value="tw" class="opt">Talks/Workshop by Hand Meet Hand</option>
            <option value="om" class="opt">Open Mic by Literaturia</option>
          </select>

        </div>
        <span style="margin: 10px 10px; color: rgba(0,0,0,.5);">* Required.</span>
        <input type="submit" name="submit" class="contact-form-submit">
        </form>
   
  </div> 
  </div>
    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </body>
</html>