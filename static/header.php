<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Surabaya Youth</title>

    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/profile.css">
    <link rel="stylesheet" href="assets/css/bas.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  </head>
  <body>

  <!-- NAVBAR -->
  <nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container-fluid">

      <a class="navbar-brand" href="#">
        <img src="assets/images/logo.png" width="100" alt="">
      </a>
      <p>Be a Better Youth</p>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse main-navbar" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="about.php">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="people.php">People</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="program.php">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contribute.php">Contribute</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="carnival.php">Carnival</a>
          </li>
        </ul>
      </div>

    </div>
  </nav>