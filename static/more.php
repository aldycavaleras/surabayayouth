<?php require_once 'header.php'; ?>




  <section id="more">
    <center> <img src="assets/images/logo.png" alt=""> </center>
      <div class="container more-text">
        <div class="row">
          <div class="col-sm-2">
            <h3>About</h3>
            <p>What we dream</p>
          </div>
          <div class="col-sm-2">
            <h3>People</h3>
            <p>Who we are</p>
          </div>
          <div class="col-sm-2">
            <h3>Program</h3>
            <p>What we do</p>
          </div>
          <div class="col-sm-2">
            <h3>Contribute</h3>
            <p>Join our team</p>
          </div>
          <div class="col-sm-2">
            <h3>Contact</h3>
            <p>Get in touch</p>
          </div>
          <div class="col-sm-2">
            <h3>Carnival</h3>
            <p>Upcoming event</p>
          </div>
        </div>
      </div>
  </section>



    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </body>
</html>
